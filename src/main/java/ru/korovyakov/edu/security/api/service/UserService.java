package ru.korovyakov.edu.security.api.service;

import ru.korovyakov.edu.security.entity.User;

public interface UserService {
    User saveUser(String login);
}
