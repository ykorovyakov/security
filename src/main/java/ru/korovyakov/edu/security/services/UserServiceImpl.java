package ru.korovyakov.edu.security.services;

import lombok.Data;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.korovyakov.edu.security.api.repository.UserRepository;
import ru.korovyakov.edu.security.api.service.UserService;
import ru.korovyakov.edu.security.entity.Role;
import ru.korovyakov.edu.security.entity.User;
import ru.korovyakov.edu.security.types.RoleType;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Optional;

@Data
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public User saveUser(final String login) {
        final var user = findUser(login);
        return
            Optional.ofNullable(user).orElseGet(() ->
            {
                userRepository.save(new User(login));
                return user;
            });
    }

    private User findUser(String login){
        return userRepository.findByLogin(login);
    }

    @PostConstruct
    private void init(){
        initUser("admin", "admin", RoleType.ROLE_ADMIN);
        initUser("user", "user", RoleType.ROLE_USER);
    }

    @Transactional
    public void initUser(String login, String password, RoleType roleType){
        final var userRaw = userRepository.findByLogin(login);
        Optional.ofNullable(userRaw).orElseGet(() ->
        {
            final var userNew = new User(login, passwordEncoder.encode(password), Collections.singleton(new Role(roleType.name())));
            userRepository.save(userNew);
            return userNew;
        });
    }
}
