package ru.korovyakov.edu.security.types;

public enum RoleType {
    ROLE_ADMIN,
    ROLE_USER;
}
